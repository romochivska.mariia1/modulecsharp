﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleApp13
{
    [Serializable]
    public class Student
    {
        public int StudentID { get; set; }
        public string LastName { get; set; }
        public string Faculty { get; set; }
        public int Course { get; set; }
        public string EnrollmentForm { get; set; }
    }

    public class ExamResult
    {
        public int StudentID { get; set; }
        public string Subject { get; set; }
        public int Grade { get; set; }
    }
    internal class Program
    {

        static void Main(string[] args)
        {
            List<Student> students = new List<Student>
        {
            new Student { StudentId = "1", LastName = "Smith", Faculty = "Engineering", Course = 1, StudyForm = "Full-time" },
            new Student { StudentId = "2", LastName = "Johnson", Faculty = "Engineering", Course = 2, StudyForm = "Part-time" },
            new Student { StudentId = "3", LastName = "Williams", Faculty = "Science", Course = 1, StudyForm = "Full-time" },
            new Student { StudentId = "4", LastName = "Brown", Faculty = "Science", Course = 3, StudyForm = "Full-time" },
            new Student { StudentId = "5", LastName = "Jones", Faculty = "Engineering", Course = 2, StudyForm = "Part-time" },
            new Student { StudentId = "6", LastName = "Davis", Faculty = "Science", Course = 2, StudyForm = "Full-time" },
            new Student { StudentId = "7", LastName = "Wilson", Faculty = "Business", Course = 1, StudyForm = "Full-time" },
            new Student { StudentId = "8", LastName = "Miller", Faculty = "Business", Course = 2, StudyForm = "Part-time" },
            new Student { StudentId = "9", LastName = "Taylor", Faculty = "Business", Course = 1, StudyForm = "Full-time" },
            new Student { StudentId = "10", LastName = "Anderson", Faculty = "Science", Course = 3, StudyForm = "Full-time" }
        };

            List<ExamResult> examResults = new List<ExamResult>
        {
            new Session { StudentId = "1", Subject = "Math", Grade = 4.5 },
            new Session { StudentId = "2", Subject = "Math", Grade = 3.8 },
            new Session { StudentId = "3", Subject = "Physics", Grade = 4.2 },
            new Session { StudentId = "4", Subject = "Chemistry", Grade = 3.9 },
            new Session { StudentId = "5", Subject = "Math", Grade = 4.1 },
            new Session { StudentId = "6", Subject = "Biology", Grade = 4.0 },
            new Session { StudentId = "7", Subject = "Economics", Grade = 4.3 },
            new Session { StudentId = "8", Subject = "History", Grade = 3.7 },
            new Session { StudentId = "9", Subject = "Computer Science", Grade = 4.6 },
            new Session { StudentId = "10", Subject = "Math", Grade = 4.4 }
        };

            var partTimeStudentsByFaculty = students
            .Where(student => student.EnrollmentForm == "Part-time")
            .GroupBy(student => new { student.Faculty, student.Course })
            .Select(group => new
            {
                Faculty = group.Key.Faculty,
                Course = group.Key.Course,
                Count = group.Count()
            });

            Console.WriteLine("Кількість студентів-заочників по факультетах та курсам:");
            partTimeStudentsByFaculty.ToList().ForEach(item =>
            {
                Console.WriteLine($"Факультет: {item.Faculty}, Курс: {item.Course}, Кількість: {item.Count}");
            });

          
            var scholarshipStudents = students
                .Where(student => student.EnrollmentForm == "Full-time" &&
                    examResults.Any(result => result.StudentID == student.StudentID && result.Grade >= 4))
                .OrderBy(student => student.Faculty)
                .Select(student => student.LastName);

            Console.WriteLine("Прізвища студентів, які отримують стипендію, впорядковані за факультетами:");
            scholarshipStudents.ToList().ForEach(lastName =>
            {
                Console.WriteLine(lastName);
            });

           
            XmlSerializer serializer = new XmlSerializer(typeof(List<Student>));
            using (StreamWriter writer = new StreamWriter("students.xml"))
            {
                serializer.Serialize(writer, students);
            }

            Console.WriteLine("Дані студентів були збережені в students.xml");

        }
    }
}
